---

# Improving Lambda Expression Story

**Estimated reading time**: 20 minutes

## Story Outline
Programming paradigms represent different models of computers and different models of thought for solving problems.  They teach us how to program a computer and which strategies we can use to implement our programs. But the paradigms apply to particular programming languages and can be mixed to obtain the better of those words. Since several years ago, the major programming languages, like C#, Java, and C++, have included some essential characteristics from other programming paradigms, as currently happed with the inclusion of anonymous functions, higher-order functions, etc.

This integration between two paradigms (object-oriented programming and functional programming) allows the emergence of new mechanisms, such as new syntactic constructors or syntactic sugar, making writing more concise code easier.

In this story, we will tell you how to improve your code that already used lambda expressions with some new constructors and syntactic sugar that enables you to code more concisely and shorter.

## Story Organization

**Story Branch**: main

&gt; `git checkout main`

**Practical task tag for self-study**: task

&gt; `git checkout task`

Tags: #lambda_expresions, #reference_methods, #streams


